#!/bin/bash
set -e
git clone https://github.com/scipopt/SCIP-SDP.git
cd SCIP-SDP
mkdir build
cd build
cmake .. -DSDPS=sdpa -DSDPA_DIR=/usr/local/sdpa.7.4.4 -DMUMPS_DIR=/builddir/SDPA/LP64/mumps/build
sed -i 's|C_INCLUDES =|C_INCLUDES = -I/builddir/scipoptsuite-9.0.0/scip/src|' src/CMakeFiles/scipsdp.dir/flags.make
sed -i 's|C_INCLUDES =|C_INCLUDES = -I/builddir/scipoptsuite-9.0.0/scip/src|' src/CMakeFiles/libscipsdp.dir/flags.make 
make
make install
