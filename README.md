# SCIP-SDP build with SDPA as Docker image

 - SCIP-SDP - https://github.com/scipopt/SCIP-SDP
 - SDPA 7.4.4 - https://github.com/sdpaninf/SDPA
 - SCIP 9.0.0 - https://www.scipopt.org/

## Use
`
docker run --rm -it -v $PWD:/workdir distcomp/scipsdp:9.0.0 scipsdp
`
