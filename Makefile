NAME=distcomp/scipsdp:9.0.0

build:
	docker build --build-arg NCPU=8 -t sdp .

push:
	docker build --build-arg NCPU=8 -t $(NAME) .
	docker push $(NAME)
