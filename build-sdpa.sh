#!/bin/bash
set -e
git clone https://github.com/sdpaninf/SDPA.git
cd SDPA/LP64
export CC=gcc
export CXX=g++
export F77=gfortran
export CFLAGS="-funroll-all-loops -O2 -m64 -fopenmp -DDIMACS_PRINT"
export CXXFLAGS="-funroll-all-loops -O2 -m64 -fopenmp -DDIMACS_PRINT"
export FFLAGS="-funroll-all-loops -O2 -m64 -fopenmp -fallow-argument-mismatch"
./configure --with-blas="-L./lib -lopenblas -lgomp" --with-lapack="-L./lib -lopenblas -lgomp"
make
./sdpa -o example1.result -dd example1.dat
make install