#!/bin/bash
set -e
VER="$1"
PREFIX="$2"
if [ $# -ne 2 ]; then
    echo "Usage: $0 <scip version> <prefix>"
    exit 1
fi

PREFIX=${PREFIX:-/usr/local}

export LIBRARY_PATH=$PREFIX/lib
export LD_LIBRARY_PATH=$PREFIX/lib

if ! [ -e scipoptsuite-$VER ]; then
    curl -L -k https://scip.zib.de/download/release/scipoptsuite-$VER.tgz | tar xz
    cd scipoptsuite-$VER
    cd scip

    mkdir -p lib/static
    #ln -sfn $PREFIX lib/static/ipopt.linux.x86_64.gnu.opt


    mkdir -p lib/include
    ln -sfn $PREFIX/include/bliss lib/include/bliss
    ln -sfn $PREFIX/lib/libbliss.a lib/static/libbliss.linux.x86_64.gnu.a

    cd ../..
fi

cd scipoptsuite-$VER
mkdir -p build
cd build

PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX -DTHREADSAFE=on -DTPI=omp -DGCG=NO -DWITH_GCG=NO -DWITH_IPOPT=NO -DIPOPT=NO

make -j $NCPU
make install
cp -vf bin/papilo $PREFIX/bin
cd ../..
