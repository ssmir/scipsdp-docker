FROM python:3.8.13-bullseye as buildoptimizer

WORKDIR /builddir

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow

RUN apt-get update \
    && apt-get install --no-install-recommends -y ca-certificates apt-utils \
    && update-ca-certificates

RUN apt-get install --no-install-recommends -y \
    pkg-config build-essential gfortran patch wget perl unzip \
    libgmp-dev zlib1g-dev libreadline-dev bison flex libncurses5-dev cmake git \
    libpthread-stubs0-dev libtbb-dev libboost-serialization-dev libboost-program-options-dev \
    libmpfrc++-dev libgsl-dev libcliquer-dev libmetis-dev file libboost-iostreams-dev \
    libopenblas-openmp-dev curl

ARG NCPU=1

COPY build-bliss.sh /builddir/
RUN bash build-bliss.sh 0.77 /usr/local

COPY build-sdpa.sh /builddir/
RUN bash build-sdpa.sh

COPY build-scip.sh /builddir/
RUN bash build-scip.sh 9.0.0 /usr/local

COPY build-scip-sdp.sh /builddir/
RUN bash build-scip-sdp.sh


FROM python:3.8.13-bullseye AS packageoptimizer

COPY --from=buildoptimizer /usr/local /usr/local
COPY --from=buildoptimizer /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

WORKDIR /workdir

RUN apt-get update && apt-get install --no-install-recommends -y \
    libgfortran5 libopenblas0-openmp libmetis5 libgomp1 libtbb2 libcliquer1 \
    libboost-serialization1.74.0 libgsl25 libboost-program-options1.74.0 \
    libncurses6 libgmpxx4ldbl libboost-iostreams1.74.0 \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV LD_LIBRARY_PATH=/usr/local/lib
