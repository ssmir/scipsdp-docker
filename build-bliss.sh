#!/bin/bash
set -e
VER="$1"
PREFIX="$2"
if [ $# -ne 2 ]; then
    echo "Usage: $0 <bliss version> <prefix>"
    exit
fi

PREFIX=${PREFIX:-/usr/local}

curl -L https://github.com/ds4dm/Bliss/archive/refs/tags/v${VER}.tar.gz | tar xz
cd Bliss-${VER}
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX
make
make install
cd ../..
rm -rf Bliss-${VER}